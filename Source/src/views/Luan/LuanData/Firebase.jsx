import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyCtCt_feoRbWCgUZpOYywAz0XOZBSwqHYs",
  authDomain: "ohcapital-aittvn.firebaseapp.com",
  databaseURL: "https://ohcapital-aittvn.firebaseio.com",
  projectId: "ohcapital-aittvn",
  storageBucket: "ohcapital-aittvn.appspot.com",
  messagingSenderId: "754812774724",
  appId: "1:754812774724:web:0c4d7682d959a351"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
// vì test thử database nên ta export database trong firebase
export const database = firebase.database();