/*!

=========================================================
* Material Dashboard PRO React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import Radio from "@material-ui/core/Radio";
import Checkbox from "@material-ui/core/Checkbox";

// @material-ui/icons
import MailOutline from "@material-ui/icons/MailOutline";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import Contacts from "@material-ui/icons/Contacts";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardText from "components/Card/CardText.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";

import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";

import firebase, { database } from './LuanData/Firebase';


class Luanlogin extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			emailTxt: "",
			passwordTXT: "",
			dataArray: [],
			authenticated: false,
		}
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentDidMount(){
    	database.ref("/").on('value', (item) => {
      
      this.setState({
        dataArray: item.val(),
        error: null,
      });
      
    });
  }

	onSubmit= (e) => {
		let x = document.getElementById("emailAddress");
		let y = document.getElementById("password");
		const email = x.value;
		const password = y.value;

		firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then((user) => {
		        this.setState(
		        	{
		        		authenticated: true
		        	}
		        )
	     	})
	     	.catch((error) => {
	       		this.setState({ error: error });
	     	});
	};


  render() {
    const { classes } = this.props;    
		       
    return ( 
    	<GridContainer>
      		{this.state.authenticated ?

	      		(
	      			<Button color="success">
	                  Login Success!
	                </Button>
                ) :
	      		(
	      			<GridItem xs={12} sm={12} md={6}>
			          <Card>
			            <CardHeader color="rose" icon>
			              <CardIcon color="rose">
			                <MailOutline />
			              </CardIcon>
			              <h4 className={classes.cardIconTitle}>Login</h4>
			            </CardHeader>
			            <CardBody>
			              <form>
			                <CustomInput labelText="Email adress" id="emailAddress" formControlProps={{fullWidth: true}} inputProps={{type: "email"}}/>
			                <CustomInput labelText="Password" id="password" formControlProps={{fullWidth: true}} inputProps={{type: "password",autoComplete: "off"}}/>
			                <Button round color="rose" onClick={this.onSubmit}>Submit</Button>
			              </form>
			            </CardBody>
			          </Card>
			        </GridItem>
	      		)
      		}
      	</GridContainer>
    );
  }
}

Luanlogin.propTypes = {
  classes: PropTypes.object
};

export default withStyles(regularFormsStyle)(Luanlogin);
