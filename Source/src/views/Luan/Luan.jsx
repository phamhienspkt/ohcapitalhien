/*!

=========================================================
* Material Dashboard PRO React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.jsx";

// material-ui icons
import Assignment from "@material-ui/icons/Equalizer";
import FindInPage from "@material-ui/icons/FindInPage";
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardHeader from "components/Card/CardHeader.jsx";

import extendedTablesStyle from "assets/jss/material-dashboard-pro-react/views/extendedTablesStyle.jsx";

import { database } from "./LuanData/Firebase";

class Luan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: [],
      sizeShow: 6,
      income: 0,
      dataArray : []
    };
    this.handleToggle = this.handleToggle.bind(this);
    this.changeSize = this.changeSize.bind(this);
  }

  componentDidMount(){
    database.ref("/").on('value', (item) => {
      
      this.setState({
        dataArray: item.val()
      });
      
    });
  }

  handleToggle(value) {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked
    });
  }

  changeSize(){
    if(this.state.sizeShow === 6)
      this.state.sizeShow = 12;
    else
      this.state.sizeShow = 6;
    this.setState(this.state);
  }

  render() {
    const { classes } = this.props;
    const fillButtons = [
      { color: "info", icon: FindInPage },
      { color: "success", icon: Edit },
      { color: "danger", icon: Close }
    ].map((prop, key) => {
      return (
        <Button color={prop.color} className={classes.actionButton} key={key}>
          <prop.icon className={classes.icon} />
        </Button>
      );
    });

    var dataBodyInvest = [];
    var dataBodyUse = [];

    this.state.income = 0;
    this.state.dataArray.map((item, key) => {
      let eleItem = [key];
      eleItem.push(item.Name);
      eleItem.push(item.Type);
      eleItem.push(item.Category);
      eleItem.push(item.Amount);
      eleItem.push(fillButtons);
      if(eleItem[2] === "Invest")
      {
        this.state.income += eleItem[4];
        eleItem[4] = "$ " + eleItem[4];
        dataBodyInvest.push(eleItem);
      }
      else
      {
        this.state.income -= eleItem[4];
        eleItem[4] = "$ " + eleItem[4];
        dataBodyUse.push(eleItem);
      }
    });
    
    
    
    const dataHead = ["#", "Name", "Type", "Category", "Amount", "Actions"];
    return (
      <div>
        <Button round color="rose" onClick={this.changeSize}>
          <Edit> Change  </Edit>
        </Button>
        
        <GridContainer>
          <GridItem xs={this.state.sizeShow}>
            <Card>
              <CardHeader color="primary" icon>
                <CardIcon color="primary">
                  <Assignment />
                </CardIcon>
                <h4 className={classes.cardIconTitle}>Invest Table</h4>
              </CardHeader>
              <CardBody>
                <Table
                  tableHead={dataHead}
                  tableData={dataBodyInvest}
                />
              </CardBody>
            </Card>
          </GridItem>     
          <GridItem xs={this.state.sizeShow}>
            <Card>
              <CardHeader color="warning" icon>
                <CardIcon color="warning">
                  <Assignment />
                </CardIcon>
                <h4 className={classes.cardIconTitle}>Use Table</h4>
              </CardHeader>
              <CardBody>
                <Table
                  tableHead={dataHead}
                  tableData={dataBodyUse}
                />
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={6} lg={6}>
                        <CustomDropdown hoverColor="info" buttonText="Dropdown" buttonProps={{ round: true, fullWidth: true, style: { marginBottom: "0" }, color: "info"}}
                          dropdownHeader="Dropdown header"
                          dropdownList={["Invest", "Use", { divider: true }]}/>
                      </GridItem>
        </GridContainer>
        <Button color="white">
          Chenh Lech: {this.state.income}$
        </Button>
      </div>
    );
  }
}

Luan.propTypes = {
  classes: PropTypes.object
};

export default withStyles(extendedTablesStyle)(Luan);
